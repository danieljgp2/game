# == Schema Information
#
# Table name: challenges
#
#  id              :integer          not null, primary key
#  number          :integer
#  opponent_number :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  user_id         :integer          not null
#
# Indexes
#
#  index_challenges_on_user_id  (user_id)
#
# Foreign Keys
#
#  user_id  (user_id => users.id)
#
class Challenge < ApplicationRecord
  belongs_to :user

  validates :number, presence: true

  def tie?
    number.eql? opponent_number
  end

  def winner?
    opponent_number.even? ? number > opponent_number : number < opponent_number
  end
end
