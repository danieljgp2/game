class CreateChallenge
  include Interactor

  def call
    challenge = Challenge.new(context.params)
    challenge.user = context.user
    challenge.opponent_number = rand(1...11)
    return context.challenge if challenge.save

    context.fail!
  end
end
