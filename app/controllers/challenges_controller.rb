class ChallengesController < ApplicationController
  before_action :authenticate_user!

  def index
    @challenges = Challenge.all.order(created_at: :desc).page(params[:page]).per(5)
  end

  def new
    @last_challenge = Challenge.last
  end

  def create
    result = CreateChallenge.call(params: challenge_params,
                                  user: current_user)
    redirect_to new_challenge_path and return if result.success?

    raise ActionController::RoutingError.new('Not Found')
  end

  private

  def challenge_params
    params.require(:challenge).permit(:number)
  end
end
