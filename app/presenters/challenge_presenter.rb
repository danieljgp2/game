class ChallengePresenter
  def initialize(challenge)
    @challenge = challenge
  end

  def winner_text
    return 'Es un empate' if @challenge.tie?

    @challenge.winner? ? '¡Ganaste esta ronda!' : 'Más suerte para la próxima. Tu contrincante es el ganador'
  end
end
