# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

# Ruby version

- 2.6.0

# System dependencies

- Rails
- Yarn or NPM
- SQLite

# Configuration

- bundle install
- yarn install

# Database creation

- rake db:create

# Database initialization

- rake db:migrate

# How to run the test suite

# Services (job queues, cache servers, search engines, etc.)

# Deployment instructions

- cap production deploy

# ...
