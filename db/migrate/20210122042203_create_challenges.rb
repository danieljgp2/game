class CreateChallenges < ActiveRecord::Migration[6.1]
  def change
    create_table :challenges do |t|
      t.references :user, null: false, foreign_key: true
      t.integer :number
      t.integer :opponent_number

      t.timestamps
    end
  end
end
