Rails.application.routes.draw do
  devise_for :users
  root 'challenges#new'

  match '/404', to: 'errors#global_error', via: :all
  match '/422', to: 'errors#global_error', via: :all
  match '/500', to: 'errors#global_error', via: :all
  resources :challenges, only: %i[new index create]
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
